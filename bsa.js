import Chat from './src/components/Chat/Chat';
import rootReducer from './src/reducers/';

export default {
  Chat,
  rootReducer,
};
