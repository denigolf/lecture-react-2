import {
  LOAD_MESSAGES,
  TOGGLE_PRELOADER,
  TOGGLE_EDIT_MODAL,
  DELETE_MESSAGE,
  SEND_MESSAGE,
  EDIT_MESSAGE,
  SET_CURRENT_MESSAGE_ID,
  DROP_CURRENT_MESSAGE_ID,
} from './actionTypes';

export const loadMessages = (messages) => {
  return {
    type: LOAD_MESSAGES,
    payload: { messages },
  };
};

export const togglePreloader = () => {
  return {
    type: TOGGLE_PRELOADER,
  };
};

export const toggleEditModal = () => {
  return {
    type: TOGGLE_EDIT_MODAL,
  };
};

export const sendMessage = (text, clearInput) => {
  return {
    type: SEND_MESSAGE,
    payload: {
      text,
      clearInput,
    },
  };
};

export const editMessage = (messageId, text) => {
  return {
    type: EDIT_MESSAGE,
    payload: {
      messageId,
      text,
    },
  };
};

export const deleteMessage = (id) => {
  return {
    type: DELETE_MESSAGE,
    payload: { id },
  };
};

export const setCurrentMessageId = (messageId) => {
  return {
    type: SET_CURRENT_MESSAGE_ID,
    payload: {
      messageId,
    },
  };
};

export const dropCurrentMessageId = () => {
  return {
    type: DROP_CURRENT_MESSAGE_ID,
  };
};
