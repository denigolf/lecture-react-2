import React, { Component } from 'react';
import { formatDateMessage } from '../../helpers';
import {
  deleteMessage,
  editMessage,
  toggleEditModal,
  setCurrentMessageId,
} from '../../actions';
import { connect } from 'react-redux';
import './OwnMessage.scss';

class OwnMessage extends Component {
  constructor(props) {
    super(props);

    this.editMessage = this.editMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
  }

  editMessage() {
    const message = this.props.message;
    const { setCurrentMessageId, toggleEditModal } = this.props;
    setCurrentMessageId(message.id);
    toggleEditModal();
  }

  deleteMessage() {
    const message = this.props.message;
    const { deleteMessage } = this.props;

    deleteMessage(message.id);
  }

  render() {
    const { message } = this.props;

    return (
      <div className="own-message">
        <div className="message-text-container">
          <span className="message-text">{message.text}</span>
          <span className="message-time">
            {formatDateMessage(message.createdAt)}
          </span>
        </div>
        <button className="message-edit" onClick={this.editMessage}>
          &#9998;
        </button>
        <button className="message-delete" onClick={this.deleteMessage}>
          &#10006;
        </button>
      </div>
    );
  }
}

const mapDispatchToProps = () => {
  return { deleteMessage, editMessage, toggleEditModal, setCurrentMessageId };
};

export default connect(null, mapDispatchToProps())(OwnMessage);
