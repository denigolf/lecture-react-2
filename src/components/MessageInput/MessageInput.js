import React, { Component } from 'react';
import { sendMessage } from '../../actions';
import { connect } from 'react-redux';
import './MessageInput.scss';

class MessageInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: '',
    };

    this.changeInputHandler = this.changeInputHandler.bind(this);
    this.clearInput = this.clearInput.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
  }

  changeInputHandler(e) {
    const inputValue = e.target.value;
    this.setState({ inputValue });
  }

  clearInput() {
    this.setState({ inputValue: '' });
  }

  sendMessage() {
    const { sendMessage } = this.props;
    sendMessage(this.state.inputValue, this.clearInput);
  }

  render() {
    const { inputValue } = this.state;

    return (
      <div className="message-input">
        <input
          className="message-input-text"
          type="text"
          placeholder="Enter your message..."
          onChange={this.changeInputHandler}
          value={inputValue}
        />
        <button className="message-input-button" onClick={this.sendMessage}>
          Send
        </button>
      </div>
    );
  }
}

const mapDispatchToProps = () => {
  return { sendMessage };
};

export default connect(null, mapDispatchToProps())(MessageInput);
