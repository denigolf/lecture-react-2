import React, { Component } from 'react';
import Preloader from '../Preloader/Preloader';
import Header from '../Header/Header';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import EditModal from '../EditModal/EditModal';
import {
  loadMessages,
  togglePreloader,
  toggleEditModal,
  setCurrentMessageId,
  dropCurrentMessageId,
} from '../../actions';
import { connect } from 'react-redux';

class Chat extends Component {
  async componentDidMount() {
    const { url, loadMessages, togglePreloader } = this.props;
    const response = await fetch(url);
    const data = await response.json();

    if (data.length === 0) {
      return;
    }

    loadMessages(data);
    togglePreloader();
    window.addEventListener('keydown', this.openModalHandler.bind(this));
  }

  openModalHandler(e) {
    const { toggleEditModal, dropCurrentMessageId, setCurrentMessageId } =
      this.props;
    const { messages, editModal } = this.props.chat;
    const ownMessages = messages.filter((message) => message.own);
    const lastOwnMessage = ownMessages[ownMessages.length - 1];

    if (editModal && e.keyCode === 27) {
      e.preventDefault();

      dropCurrentMessageId();
      toggleEditModal();
    }

    if (e.keyCode === 38 && lastOwnMessage) {
      e.preventDefault();

      setCurrentMessageId(lastOwnMessage.id);
      toggleEditModal();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.openModalHandler.bind(this));
  }

  render() {
    const { messages, preloader, editModal } = this.props.chat;
    return preloader ? (
      <Preloader />
    ) : (
      <div className="chat">
        <Header messages={messages} />
        <MessageList messages={messages} />
        <MessageInput data={messages} />
        {editModal ? <EditModal /> : null}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = () => {
  return {
    loadMessages,
    togglePreloader,
    toggleEditModal,
    setCurrentMessageId,
    dropCurrentMessageId,
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(Chat);
