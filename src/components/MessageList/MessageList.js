import React, { Component } from 'react';
import Message from '../Message/Message';
import OwnMessage from '../OwnMessage/OwnMessage';
import { formatDateDivider } from '../../helpers';

import './MessageList.scss';

class MessageList extends Component {
  constructor(props) {
    super(props);

    this.getGroupedMessages = this.getGroupedMessages.bind(this);
  }

  getGroupedMessages() {
    const groupedMessages = {};

    this.props.messages.forEach((message) => {
      const date = new Date(message.createdAt).toISOString().slice(0, 10);
      if (!groupedMessages[date]) {
        groupedMessages[date] = [message];
      } else {
        groupedMessages[date] = [...groupedMessages[date], message];
      }
    });

    return groupedMessages;
  }

  render() {
    const groupedMessages = this.getGroupedMessages();

    const groupedMessagesRender = Object.entries(groupedMessages).map(
      (group) => {
        const [date, messages] = group;

        const today = new Date().toISOString().slice(0, 10);

        let yesterday = new Date(today);
        yesterday.setDate(new Date(today).getDate() - 1);
        yesterday = yesterday.toISOString().slice(0, 10);

        let divider = null;

        switch (date) {
          case today:
            divider = 'Today';
            break;
          case yesterday:
            divider = 'Yesterday';
            break;
          default:
            divider = formatDateDivider(date);
            break;
        }
        return (
          <div className="message-list-group" key={date}>
            <div className="messages-divider-container">
              <span className="messages-divider">{divider}</span>
            </div>
            {messages.map((message) => {
              if (message.own) {
                return (
                  <OwnMessage
                    message={message}
                    deleteMessage={this.props.deleteMessage}
                    editMessage={this.props.editMessage}
                    key={message.id}
                  />
                );
              }
              return <Message message={message} key={message.id} />;
            })}
          </div>
        );
      }
    );

    return <div className="message-list">{groupedMessagesRender}</div>;
  }
}

export default MessageList;
